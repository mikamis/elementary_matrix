package jp.kobo;

import java.util.ArrayList;
import java.util.List;

public class Matrix {
	int[][] mMat;
	Mat_Log mLog = new Mat_Log();

	public Matrix(int[][] aMatrix) {
		// TODO Auto-generated constructor stub
		mMat = aMatrix;
		setMatLog();
	}

	public int getMat(int aRow, int aColumn) {
		return mMat[aRow][aColumn];
	}

	public void change(int aDoRow, int aDoneRow, int aScalar) {
		for (int i = 0; i < mMat[0].length; i++) {
			mMat[aDoneRow][i] += aScalar * mMat[aDoRow][i];
		}
		setMatLog();
	}

	public void convert(int aRow1, int aRow2) {
		int[] tRow = mMat[aRow1];
		mMat[aRow1] = mMat[aRow2];
		mMat[aRow2] = tRow;
		setMatLog();
	}

	public void scalar(int aRow, int aScalar) {
		for (int i = 0; i < mMat[0].length; i++) {
			mMat[aRow][i] *= aScalar;
		}
		setMatLog();
	}

	public Mat_Log getmLog() {
		return mLog;
	}

	public void setMatLog() {
		int[][] tLog = new int[mMat.length][];
		for (int i = 0; i < tLog.length; i++) {
			tLog[i] = mMat[i].clone();
		}
		mLog.setMat(tLog);
	}
}
