package jp.kobo;

public interface Table {
/**
 * 二次元表となるデータを渡します。 
 *
 */
    public String getElemet(int aColumn,int aRow);
    public int getNumOfColumn();
    public int getNumOfRow();
}


